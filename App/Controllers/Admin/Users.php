<?php

namespace App\Controllers\Admin;

use Core\Controller;

/**
 * User admin controller
 *
 */
class Users extends Controller {
	/**
	 * Show the index page
	 *
	 * @return void
	 */
	public function indexAction() {
		echo 'User admin index';
	}
}
