<?php

/**
 * Front controller
 *
 */
/**
 * Composer Autoloader
 *
 */
$autoload_dir = dirname( __DIR__ ) . '/vendor/autoload.php';

if ( ! file_exists( $autoload_dir ) ) {
	throw new Exception( 'Please, run composer install to download and install the dependencies.' );
}

require_once $autoload_dir;

require '../vendor/autoload.php';

/**
 * Error and Exception handling
 */
error_reporting(E_ALL);
set_error_handler('Core\Error::errorHandler');
set_exception_handler('Core\Error::exceptionHandler');

/**
 * Routing
 */

$router = new Core\Router();

// Add the routes
$router->add('', ['controller' => 'Home', 'action' => 'index']);
$router->add('{controller}/{action}');
$router->add('{controller}/{id:\d+}/{action}');
$router->add('admin/{controller}/{action}', ['namespace' => 'Admin']);

$router->dispatch( $_SERVER['QUERY_STRING'] );


